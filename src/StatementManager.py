from IfStatement import IfStatement
from GotoStatement import GotoStatement
from PrintStatement import PrintStatement
from LetStatement import LetStatement
from RemStatement import RemStatement
from State import State

class StatementManager(object):
    """ Stores and manipulates statement lines 

    """
    PRINT_COMMAND_STRING = "PRINT"
    REMARK_COMMAND_STRING = "REM"
    LET_COMMAND_STRING = "LET"
    GOTO_COMMAND_STRING = "GOTO"
    IF_COMMAND_STRING = "IF"


    def __init__(self, state):
        """ Initialises a StatementManager """
        self.state = state
        self.statements = []
        self.statementLineNumbers = {}

    def hasNext(self):
        """ Checks that the next statement index to execute is within
        the bounds of the statements array """

        return (self.state.getNextStatementIndex() < len(self.statements));

    def next(self):
        """ Returns the next statement to execute and increments the
        next statement index"""
        statement = self.statements[self.state.getNextStatementIndex()]
        self.state.incrementNextStatementIndex()
        
        return statement

    def load(self,statementStringArray):
        """Reads in an array of newline separated statements and prepares two
        arrays: one holding an array of statements ordered by line
        number and the other mapping of statement line to index in the
        first array

        """

        # Sort the statements by line number
        statementStringArray.sort(key=lambda statementString: int(statementString.split()[0]))
        # Convert text statements into objects
        for statementIndex, statementString in enumerate(statementStringArray):
            components = statementString.strip("\n").split()

            if (components[1] == self.PRINT_COMMAND_STRING):
                newStatement = PrintStatement(self, components[0], ' '.join(components[2:]))
            elif (components[1] == self.LET_COMMAND_STRING):
                newStatement = LetStatement(self, components[0], ' '.join(components[2:]))
            elif (components[1] == self.GOTO_COMMAND_STRING):
                newStatement = GotoStatement(self, components[0], ' '.join(components[2:]))
            elif (components[1] == self.IF_COMMAND_STRING):
                newStatement = IfStatement(self, components[0], ' '.join(components[2:]))
            elif (components[1] == self.REMARK_COMMAND_STRING):
                newStatement = RemStatement(self, None, None)

            # Add an entry to the mapping table for fast lookup
            self.statementLineNumbers[components[0]] = statementIndex
            self.statements.append(newStatement)

    def getStatementList(self):
        return self.statements

    def getStatementLineNumbersList(self):
        return self.statementLineNumbers

    def setNextStatementIndexFromLineNumber(self, lineNumber):
        """ Uses a map of line numbers to statement indexes to set the
        next statement index for execution """

        if (lineNumber in self.statementLineNumbers):
            self.state.setNextStatementIndex(self.statementLineNumbers[lineNumber])

if __name__ == '__main__':
    state = State()
    statementManager = StatementManager(state)

    print("StatementManager having next should should be False, actually " + str(statementManager.hasNext()))

    printStatement = "10 PRINT X"
    gotoStatement = "7 GOTO 9"
    letStatement0 = "5 LET X = 10 + 0"
    letStatement1 = "8 LET X = X + 1"
    letStatement2 = "9 LET X = X + X"
    ifStatement = "7 IF X == 10 GOTO 9"
    remStatement = "11 This is a comment"

    statementManager.load([ifStatement, letStatement0, printStatement, letStatement2, letStatement1])
    print("Loaded statements: " + str(statementManager.getStatementList()))
    print("Loaded statement line numbers: " + str(statementManager.getStatementLineNumbersList()))
    print("StatementManager having next should should be True, actually " + str(statementManager.hasNext()))
    while (statementManager.hasNext()):
        statementManager.next().execute(state)


