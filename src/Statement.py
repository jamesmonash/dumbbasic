class Statement(object):
    """ Class representing a dumbbasic statement """
    
    def __init__(self, statementManager, lineNumber, statementArgumentsString):
        """Statement for executing a print command """
        self.statementManager = statementManager
        self.lineNumber = lineNumber
        self.statementArgumentsString = statementArgumentsString


    def execute(self, state):
        """ Dummy method for statement execution """
        pass

    def getLineNumber(self):
        return self.lineNumber

    def getStatementArgumentsString(self):
        return self.statementArgumentsString

if __name__ == '__main__':

    printStatement = "10 PRINT X"
    components = printStatement.split()
    statement = Statement(components[0], ' '.join(components[2:]))
    print("Expected line number: " + str(components[0]) + ", actual " + str(statement.getLineNumber()))
    print("Expected statement arguments: '" + str(' ' .join(components[2:])) + "', actual '" + str(statement.getStatementArgumentsString()) + "'") 
