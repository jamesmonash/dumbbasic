from Statement import Statement

class PrintStatement(Statement):
    """ Class representing a print statement """

    def execute(self, state):
        arguments = self.statementArgumentsString.split()

        variableValue = state.getVariableValue(arguments[0])
        if (variableValue == False):
            print(arguments[0])
        else:
            print(variableValue)        
