from Statement import Statement

class GotoStatement(Statement):

    def execute(self, state):
        arguments = self.statementArgumentsString.split()
        self.statementManager.setNextStatementIndexFromLineNumber(arguments[0])
