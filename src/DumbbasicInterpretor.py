#!/usr/bin/python

from State import State 
from StatementManager import StatementManager

class DumbbasicInterpretor(object):
    """ Class for interpreting a group of dumbbasic statements """

    def __init__(self):
        """Initialises the state and statement manager for this
        interpretor """
        self.state = State()
        self.statementManager = StatementManager(self.state)

    def readStatements(self,statementArray):
        """ Execute an array of dumbbasic statements """
        self.statementManager.load(statementArray)

    def executeStatements(self):
        """ Executes loaded statements until either an error occurs or
        there are no statements left """

        while (self.statementManager.hasNext()):
            self.statementManager.next().execute(self.state)

if __name__ == '__main__':
    DumbbasicInterpretor()
