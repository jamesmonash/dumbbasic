class State(object):
    """ Stores and allows manipulation of current system state """

    def __init__(self):
        self.nextLineNumber = None
        self.nextStatementIndex = 0
        self.variables = {}

    def getNextLineNumber(self):
        return self.nextLineNumber

    def setNextLineNumber(self, value):
        self.nextLineNumber = value

    def getNextStatementIndex(self):
        """ Gets the value of nextStatementIndex """
        return self.nextStatementIndex

    def setNextStatementIndex(self, value):
        """ Sets the value of nextStatementIndex """
        self.nextStatementIndex = value

    def incrementNextStatementIndex(self):
        """ Increments the current line index by one """
        self.nextStatementIndex += 1

    def getVariableValue(self, varName):
        """Returns the value of a variable if set. If the variable is not set
        initialises it to zero and returns that.

        """
        if (not varName in self.variables):
            self.variables[varName] = 0
            
        return self.variables[varName]


    def setVariableValue(self, varName, value):
        """ Sets the value of a variable """
        self.variables[varName] = value


if __name__ == '__main__':
    # perform some basic testing
    state = State()

    print("Statement index should be 0, actually " + str(state.getNextStatementIndex()))
    state.setNextStatementIndex(1)
    print("Statement index should be 1, actually " + str(state.getNextStatementIndex()))
    state.incrementNextStatementIndex()
    print("Statement index should be 2, actually " + str(state.getNextStatementIndex()))

    varName = "var"
    varValue = "value"
    print("Value of variable \"var\" should be None, actually " + str(state.getVariableValue(varName)))
    state.setVariableValue(varName,varValue)
    print("Value of variable \"var\" should be " + varValue + ", actually " + str(state.getVariableValue(varName)))
