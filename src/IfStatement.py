from Statement import Statement
import utility

class IfStatement(Statement):

    def execute(self, state):
        arguments = self.statementArgumentsString.split()

        if (not utility.is_integer(arguments[0])):
            arguments[0] = str(state.getVariableValue(arguments[0]))
        if (not utility.is_integer(arguments[2])):
            arguments[2] = str(state.getVariableValue(arguments[2]))

        isEqual = eval(' '.join(arguments[0:3]))

        if (isEqual):
            self.statementManager.setNextStatementIndexFromLineNumber(arguments[4])



    
