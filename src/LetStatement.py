from Statement import Statement
import utility

class LetStatement(Statement):

    def execute(self, state):
        arguments = self.statementArgumentsString.split()

        if (not utility.is_integer(arguments[2])):
            arguments[2] = str(state.getVariableValue(arguments[2]))
        if (not utility.is_integer(arguments[4])):
            arguments[4] = str(state.getVariableValue(arguments[4]))

        state.setVariableValue(arguments[0], eval(' '.join(arguments[2:])))
