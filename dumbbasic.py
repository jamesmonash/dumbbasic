"""Driver for dumbbasic interpretor

Author: James Eggington
Id: 22612211

Overview
The interpretor is broken into several components:

- The DumbbasicInterpretor class which reads and executes dumbbasic
  statements.

- The StatementManager class which organises statements, keeps track
  of which statement is currently executing and maps line number to
  index in the list of statements.

- The State class which keeps track of variable values.

- A Statement class and various subclasses which represent supported
  statements.

"""

import sys
from src import DumbbasicInterpretor

if __name__ == '__main__':
    interpretor = DumbbasicInterpretor()
    interpretor.readStatements(sys.stdin.readlines())
    interpretor.executeStatements()
    
